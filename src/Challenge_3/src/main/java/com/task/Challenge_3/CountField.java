package Challenge_3.src.main.java.com.task.Challenge_3;

import java.util.List;
import java.util.stream.Collectors;

public class CountField {
    final private double mean;
    private double median;
    private double modus;
    private int[] modusArray = new int[10];

    public double getMean() {
        return mean;
    }

    public double getMedian() {
        return median;
    }

    public double getModus() { return modus; }

    public int[] getModusArray() {
        return modusArray;
    }

    public CountField(List<Integer> datas) {
        // Set Mean
        this.mean = datas.stream().mapToInt(data->data).average().orElse(0.0);
        //Sort Data
        if(datas.size() != 0){
            List<Integer> dataSorted = datas.stream().sorted().collect(Collectors.toList());
            countModus(dataSorted);
            countMedian(dataSorted);
        }else{
            this.median = 0.0;
            this.modus =0.0;
        }

    }

    private void countMedian(List<Integer> datas) {
        double total;
        int datasLength = datas.size();

        if((datasLength % 2) == 0){
            // Rumus Genap
            int getCenterIndex = datasLength/2;
            double val1 = datas.get(getCenterIndex);
            double val2 = datas.get(getCenterIndex + 1);
            total = (val1 + val2) / 2;
        }else{
            // Rumus Ganjil
            total = datas.get(datasLength / 2);
        }

      this.median = total;
    }

    private void countModus(List<Integer> datas) {
        int[] modusArray = new int[10];
        for (int data : datas ) {
           modusArray[data - 1]++;
        }
        int highValue = 0;
        int indexValue = 0;
        for (int i = 0; i < modusArray.length; i++) {
            if(highValue < modusArray[i]){
                highValue = modusArray[i];
                indexValue = ++i;
            }
        }

        this.modusArray = modusArray;
        this.modus      =  indexValue;
    }

}
