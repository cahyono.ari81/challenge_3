package Challenge_3.src.test.java.com.task.Challenge_3;

import Challenge_3.src.main.java.com.task.Challenge_3.CountField;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class ReadFileTest {
    @Test
    @DisplayName("Positive Test getMean")
    public void testMeanPositive() {
        List<Integer> list = Arrays.asList(7, 7, 7);
        CountField countField = new CountField(list);
        Assertions.assertEquals(7.0, countField.getMean());
    }

    @Test
    @DisplayName("Positive Test getModus")
    public void testModusPositive() {
        List<Integer> list = Arrays.asList(1, 9,3 ,4 ,4 ,4 ,4, 7, 7, 7);
        CountField countField = new CountField(list);
        Assertions.assertEquals(4.0, countField.getModus());
    }

    @Test
    @DisplayName("Positive Test getMedian")
    public void testMedianPositive() {
        List<Integer> list = Arrays.asList(1, 9, 3 ,4, 4, 7, 7, 7);
        CountField countField = new CountField(list);
        Assertions.assertEquals(7.0, countField.getMedian());
    }

    @Test
    @DisplayName("Negative Test getMean")
    public void testMeanNegative() {
        List<Integer> list = Collections.emptyList();
        CountField countField = new CountField(list);
        Assertions.assertEquals(0.0, countField.getMean());
    }

    @Test
    @DisplayName("Negative Test getModus")
    public void testModusNegative() {
        List<Integer> list = Collections.emptyList();
        CountField countField = new CountField(list);
        Assertions.assertEquals(0.0, countField.getModus());
    }

    @Test
    @DisplayName("Negative Test getMedian")
    public void testMedianNegative() {
        List<Integer> list = Collections.emptyList();
        CountField countField = new CountField(list);
        Assertions.assertEquals(0.0, countField.getMedian());
    }
}
